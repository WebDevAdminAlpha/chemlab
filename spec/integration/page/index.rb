# frozen_string_literal: true

module Chemlaboratory
  module Page
    class Index < Chemlab::Page
      url 'index.html'

      div :header
      h1 :header_title
    end
  end
end
