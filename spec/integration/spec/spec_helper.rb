# frozen_string_literal: true

require 'watir'
require 'airborne'
require 'active_support/time'
require 'active_support/number_helper'
require_relative '../libraries'

Dir['./spec/shared_examples/*.rb'].sort.each { |f| require f }

RSpec.configure do |config|
  # rspec-expectations config goes here. You can use an alternate
  # assertion/expectation library such as wrong or the stdlib/minitest
  # assertions if you prefer.
  config.expect_with :rspec do |expectations|
    # This option will default to `true` in RSpec 4. It makes the `description`
    # and `failure_message` of custom matchers include text for helper methods
    # defined using `chain`, e.g.:
    #     be_bigger_than(2).and_smaller_than(4).description
    #     # => "be bigger than 2 and smaller than 4"
    # ...rather than:
    #     # => "be bigger than 2"
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended, and will default to
    # `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end

  # This option will default to `:apply_to_host_groups` in RSpec 4 (and will
  # have no way to turn it off -- the option exists only for backwards
  # compatibility in RSpec 3). It causes shared context metadata to be
  # inherited by the metadata hash of host groups and examples, rather than
  # triggering implicit auto-inclusion in groups with matching metadata.
  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.define_derived_metadata(file_path: Regexp.new('/spec/api')) do |metadata|
    metadata[:type] = :api
  end

  config.define_derived_metadata(file_path: Regexp.new('/spec/ui')) do |metadata|
    metadata[:type] = :ui
  end

  config.before(:each, type: :ui) do
    @site = QA::Runtime::Site.new
  end

  if QA::Runtime::Env.running_in_ci?
    Dir.mkdir('tmp') unless Dir.exist?('tmp')
    config.before(:suite) do
      Watir.logger.level = :debug
      Watir.logger.output = "tmp/watir-#{time_suffix}.log"
    end

    config.after(:each, type: :ui) do |example|
      if example.exception
        screenshot = "tmp/#{example.description.tr(' ', '_')}-#{time_suffix}.png"
        @site.save_screenshot screenshot
      end

      @site.quit
    end
  else
    config.after(:each, type: :ui) do
      @site.quit
    end
  end
end

Airborne.configure do |config|
  config.match_expected_default = true
  config.match_actual_default = false
  config.verify_ssl = false
end

private

def time_suffix
  Time.current.strftime("%y%m%d%H%M")
end
