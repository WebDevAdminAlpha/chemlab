# frozen_string_literal: true

module Chemlaboratory
  RSpec.describe 'Index' do
    it 'renders a header title' do
      Page::Index.perform do |index|
        expect(index.header_title).to eq('Welcome to the Lab.')
      end
    end
  end
end
