# frozen_string_literal: true

require_relative '../../lib/chemlab'

module Chemlaboratory
  module Page
    autoload :Index, 'page/index'
  end
end
