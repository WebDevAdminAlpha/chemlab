# frozen_string_literal: true

require 'watir'

# Chemlaboratory
module Chemlab

  autoload :Version, 'chemlab/version'

  # Yields the global configuration to a block.
  # @yield [Configuration] global configuration
  #
  # @example
  #     Chemlab.configure do |config|
  #       config.base_url = 'https://example.com'
  #     end
  def self.configure
    yield configuration if block_given?
  end

  # Returns the global [Configuration](Configuration) object. While
  # you _can_ use this method to access the configuration, the more common
  # convention is to use Chemlab.configure.
  #
  # @example
  #     Chemlab.configuration.drb_port = 1234
  # @see Chemlab.configure
  def self.configuration
    @configuration ||= Chemlab::Configuration.new
  end

  autoload :Configuration, 'chemlab/configuration'
  autoload :Attributable, 'chemlab/attributable'

  autoload :Element, 'chemlab/element'
  autoload :Component, 'chemlab/component'
  autoload :Page, 'chemlab/page'

  autoload :ApiFabricator, 'chemlab/api_fabricator'
  autoload :Resource, 'chemlab/resource'

  # Runtime modules
  module Runtime
    autoload :Env, 'chemlab/runtime/env'
    autoload :Browser, 'chemlab/runtime/browser'
  end

  # Support modules
  module Support
    autoload :API, 'chemlab/support/api'
  end
end
