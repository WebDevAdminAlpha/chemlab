# frozen_string_literal: true

require 'airborne'

module Chemlab
  module Runtime
    module API
      class Client
        attr_reader :address, :user

        def initialize(address = :gitlab, personal_access_token: nil, is_new_session: true, user: nil, ip_limits: false)
          @address = address
          @user = user
        end
      end
    end
  end
end
