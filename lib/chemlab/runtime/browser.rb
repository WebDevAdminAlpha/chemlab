# frozen_string_literal: true

module Chemlab
  module Runtime
    # The browser configurator
    class Browser
      extend SingleForwardable
      attr_accessor :session
      attr_reader :browser_options

      def initialize(browser_options)
        @browser_options = browser_options
        @session = Session.new(browser_options)
      end

      def self.navigate_to(page_class)
        Chemlab.configuration.browser.navigate_to(page_class.path)
      end

      def navigate_to(path)
        @session ||= Chemlab.configuration.browser.session
        @session.engine.goto(Chemlab.configuration.base_url + path)
      end

      # An individual session
      class Session
        attr_reader :engine

        def initialize(browser)
          @engine = Watir::Browser.new(*browser)

          # @engine.goto(Chemlab.configuration.base_url)
        end

        def current_url
          engine.url
        end

        def refresh
          engine.refresh
        end

        def text
          engine.text
        end

        def quit
          engine.close
        end

        def save_screenshot(file_name)
          engine.screenshot.save(file_name)
        end

        def wait_until(timeout, message = nil, &block)
          engine.wait_until(timeout: timeout, message: message, &block)
        end

        def wait_while(timeout, message = nil, &block)
          engine.wait_while(timeout: timeout, message: message, &block)
        end
      end
    end
  end
end
