# frozen_string_literal: true

module Chemlab
  module Runtime
    # Environment configuration file
    module Env
      module_function

      def debug?
        enabled?(ENV['DEBUG'], default: false)
      end

      private

      def enabled?(value, default: true)
        return default if value.nil?

        (value =~ /^(false|no|0)$/i) != 0
      end
    end
  end
end
