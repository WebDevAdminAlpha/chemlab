# frozen_string_literal: true

module Chemlab
  module Element
    CLICKABLES = %i[
      a
      button
      link
      checkbox
      image
      radio
    ].freeze

    SELECTABLES = %i[
      select
    ].freeze

    INPUTS = %i[
      text_field
      text_area
    ].freeze
  end
end
