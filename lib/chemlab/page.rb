# frozen_string_literal: true

module Chemlab
  # Representation of a Page on the UI
  class Page < Component

    attribute :path

    def visit
      Runtime::Browser.navigate_to(self.class)
    end
  end
end
