# frozen_string_literal: true

module Chemlab
  # Attributable module
  # @example
  #   class MyClass
  #     include Attributable
  #     attribute :url
  #     attribute :id
  #     attribute :name { 'test' }
  module Attributable
    def attribute(name)
      default_value = nil
      default_value = yield if block_given?

      define_method(name) do
        instance_variable_get("@#{name}") ||
          instance_variable_set(
            "@#{name}",
            default_value
          )
      end
    end
  end
end
