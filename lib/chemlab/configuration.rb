# frozen_string_literal: true

module Chemlab
  # Chemlab Configuration
  class Configuration
    # Add a chemlab configuration
    def self.add_config(name)
      attr_accessor name
    end

    add_config :base_url

    attr_reader :browser
    # Set the browser and browser arguments Chemlab should use
    def browser=(browser)
      @browser = Runtime::Browser.new(browser)
    end

    attr_reader :libraries
    # Specify which libraries to load
    def libraries=(libraries = [])
      @libraries = Chemlab.const_set('Vendor', Module.new)

      libraries.each do |library|
        @libraries.const_set(library.to_s, library)
      end
    end

    # Call RSpec.configure for additional configuration
    def configure_rspec
      RSpec.configure do |rspec|
        yield rspec if block_given?

        # TODO Change this. /spec/api /spec/ui is hardcoded
        rspec.define_derived_metadata(file_path: Regexp.new('/spec/api')) do |metadata|
          metadata[:type] = :api
        end

        rspec.define_derived_metadata(file_path: Regexp.new('/spec/ui')) do |metadata|
          metadata[:type] = :ui
        end

        rspec.after(:each, type: :ui) do
          Chemlab.configuration.browser&.session&.engine&.quit
        end
      end
    end
  end
end
