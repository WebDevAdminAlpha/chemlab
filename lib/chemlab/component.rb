# frozen_string_literal: true

module Chemlab
  # The base representation of *any* UI component.
  class Component
    extend SingleForwardable

    def_delegator :browser, :browser
    def_delegators :evaluator, :attribute

    def self.perform
      yield new if block_given?
    end

    class << self
      Watir::Container.instance_methods(false).each do |watir_method|
        define_method watir_method do |name, *args, &block|
          next if public_instance_methods.include? name

          define_method("#{name}_element") do
            find_element(watir_method, name, args.first, &block)
          end

          define_method("#{name}?") do
            send("#{name}_element").present?
          end

          define_method(name) do
            element = send("#{name}_element")
            if Element::CLICKABLES.include? watir_method
              element.wait_until(&:present?).click
            elsif Element::INPUTS.include? watir_method
              element.value
            else
              element.text
            end
          end

          define_method("#{name}=") do |val|
            if Element::SELECTABLES.include? watir_method
              send("#{name}_element").select val
            else
              send("#{name}_element").set val
            end
          end
        end
      end
    end

    class DSL
      def initialize(base)
        @base = base
      end

      def attribute(name)
        @base.module_eval do
          attr_writer(name)

          default_value = block_given? ? yield : nil

          define_singleton_method(name) do |value = nil|
            instance_variable_get("@#{name}") ||
              instance_variable_set(
                "@#{name}",
                value || default_value
              )
          end
        end
      end
    end

    def self.evaluator
      @evaluator ||= DSL.new(self)
    end
    private_class_method :evaluator

    private

    def find_element(watir_method, name, locator = nil, &block)
      locator = { css: %([data-qa-selector="#{name}"]) } if locator.nil?

      return instance_exec(&block) if block_given?

      Chemlab.configuration.browser.session.engine.send(watir_method, locator).wait_until(&:exist?)
    end
  end
end
