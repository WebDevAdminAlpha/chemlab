# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'chemlab/version'

Gem::Specification.new do |spec|
  spec.name = 'chemlab'
  spec.version = Chemlab::VERSION
  spec.authors = ['GitLab Quality']
  spec.email = ['quality@gitlab.com']

  spec.summary = 'Automation framework built by GitLab, for the world.'
  spec.homepage = 'https://about.gitlab.com'
  spec.license = 'MIT'

  spec.files = `git ls-files -- lib/*`.split("\n")

  # TODO: eventually make a chemlab [...args] executable
  # spec.bindir = 'exe'
  # spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }

  spec.require_paths = ['lib']

  # Some dependencies are pinned, to prevent new cops from breaking the CI pipelines
  spec.add_development_dependency 'debase', '~> 0.2.4.1'
  spec.add_development_dependency 'faker', '~> 1.6', '>= 1.6.6'
  spec.add_development_dependency 'pry-byebug', '~> 3.5.1'
  spec.add_development_dependency 'ruby-debug-ide', '~> 0.7.0'
  spec.add_development_dependency 'timecop', '~> 0.9.1'
  spec.add_development_dependency 'rspec', '~> 3.7'
  spec.add_development_dependency 'rspec-retry', '~> 0.6.1'
  spec.add_development_dependency 'rspec_junit_formatter', '~> 0.4.1'
  spec.add_development_dependency 'nokogiri', '~> 1.10.9'

  # spec.add_runtime_dependency 'airborne', '~> 0.2.13'
  spec.add_runtime_dependency 'rake', '~> 12.3.0'
  spec.add_runtime_dependency 'selenium-webdriver', '~> 3.12'
  spec.add_runtime_dependency 'watir', '~> 6.17'
end
