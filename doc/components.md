# Components

Components are the building blocks of any view within the UI.

A component could be one of, but not limited to the following:

- [Pages](pages.md)
- Layers
- Modals
- Sections
- Forms
