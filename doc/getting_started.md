# Getting Started with Chemlab

> This guide will 

## Add Chemlab to your project

**Gemfile**
```ruby
gem 'chemlab'
```

**spec/spec_helper.rb**

## Example Chemlab library structure

> *Note* This project structure assumes you are creating a new End-to-end test framework for
> a project within GitLab.
```
project/
  qa/
    component/
      navbar.rb
    flow/
      login.rb
    page/
      login.rb
    spec/
      api/
      ui/
        login/
          login_spec.rb
      unit/
      spec_helper.rb
    .rspec
    qa.rb
```

### [`component/`](components.md)

### [`page/`](pages.md)

### [`flow/`](flows.md)

### `spec/`

> All tests should reside within this directory

#### `spec/api/`

> API-specific tests.  These tests will not touch the UI.

#### `spec/unit/`

> Specs written to test local framework-specific functionality.

#### `spec/spec_helper.rb`

> spec_helper.rb should be included within all specs written, sans [`spec/unit/`](#spec-unit)
> This file should also configure Chemlab with the desired browser, Application Under Test (AUT) base URL.
> This file would also be used for configuring RSpec.

```ruby
require 'chemlab'

Chemlab.configure do |chemlab|
  chemlab.browser = :chrome, { headless: true }
  chemlab.base_url = 'http://example.com'

  chemlab.configure_rspec do |rspec|
    # configure RSpec here
  end
end
```

# `qa/qa.rb`

> Module used to autoload all page and component libraries

```ruby
module QA
  module Page
    autoload :Login, 'page/login'
  
    module Users
      autoload :Index, 'page/users/index'
    end
  end
  
  module Component
    autoload :Navbar, 'component/navbar'
  end
end
```
