# Pages

A Page is a representation of a complete page within the UI.

Given the following HTML:

```html
<!DOCTYPE html>
<html>
<body>
  <input type="text" data-qa-selector="username" />
  <input type="password" data-qa-selector="password" />
  <input type="button" value="Log in" data-qa-selector="login" />
</body>
</html>
```

A Page Library representation of this in Chemlab would look like:

```ruby
module Page
  class Login < Chemlab::Page
    url '/users/sign_in'

    text_field :username
    text_field :password
    button :login
  
    def sign_in_as(username, password)
      self.username = username
      self.password = password
   
      login
    end
  end 
end
``` 

## Attributes

### `url`

> The locator of the page, with a preceding slash (`/`)

Given these URLs:

```ruby
# https://example.com/users/sign_in
class Login < Chemlab::Page
  url '/users/sign_in'
end

# https://example.com/resources/new
class New < Chemlab::Page
  url '/resources/new'
end
```

## Methods

### `visit`

> Navigate to the page by forcing a URL redirect to the Page's [`url` attribute](#url)
